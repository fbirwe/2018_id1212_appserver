document.addEventListener("DOMContentLoaded", ev => {
    resize();
})

window.addEventListener('resize', ev => {
    resize();
})

function resize() {
    const mainWrapper = document.querySelector('#main_wrapper');

    mainWrapper.style.width = window.innerWidth + 'px';
    mainWrapper.style.height = window.innerHeight + 'px';
}