package se.kth.id1212.applicationServers.currency.domain;

import java.util.List;

public interface ConversionDTO {

    public CurrencyDTO getFrom();
    public CurrencyDTO getTo();
    public double getValue();
    public long getTimestamp();

    public String toString();

}
