package se.kth.id1212.applicationServers.currency.domain;

public interface CurrencyDTO {

    public String getIsoId();

    public String getName();

    public double getToDollar();

    public String toString();
}
