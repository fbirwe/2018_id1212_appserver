package se.kth.id1212.applicationServers.currency.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import se.kth.id1212.applicationServers.currency.application.CurrencyManagement;
import se.kth.id1212.applicationServers.currency.domain.Currency;
import se.kth.id1212.applicationServers.currency.domain.CurrencyDTO;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class CurrencyController {

    @Autowired
    CurrencyManagement currencyManagement;


    @GetMapping("/")
    public String showClient(ConversionForm conversionForm, Model model) {
        model.addAttribute("currencies", currencyManagement.getAllCurrencies());

        return "client";
    }

    @PostMapping("/")
    public String checkConversion(@Valid ConversionForm conversionForm, BindingResult bindingResult, Model model) {

        model.addAttribute("currencies", currencyManagement.getAllCurrencies());


        if (bindingResult.hasErrors()) {
            return "client";
        }

        model.addAttribute( "result", currencyManagement.calculate( currencyManagement.getCurrency( conversionForm.getFrom_currency()), currencyManagement.getCurrency( conversionForm.getTo_currency() ), conversionForm.getCurrency_value() ) );

        return "client";
    }


    @GetMapping("/admin")
    public String showAdmin(CurrencyForm currencyForm, Model model) {
        model.addAttribute("currencies", currencyManagement.getAllCurrencies() );
        model.addAttribute("conversionCount", currencyManagement.getConversionCount());

        return "admin";
    }

    @PostMapping("/admin")
    public String checkCurrency(@Valid CurrencyForm currencyForm, BindingResult bindingResult, Model model) {
        model.addAttribute("conversionCount", currencyManagement.getConversionCount());

        if (bindingResult.hasErrors()) {
            model.addAttribute("currencies", currencyManagement.getAllCurrencies() );


            return "admin";
        }

        currencyManagement.addCurrency( currencyForm.getIso_id(), currencyForm.getName(), currencyForm.getTo_dollar() );
        model.addAttribute("currencies", currencyManagement.getAllCurrencies() );

        return "admin";
    }
}
