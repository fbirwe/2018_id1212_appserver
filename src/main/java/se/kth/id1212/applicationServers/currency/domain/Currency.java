package se.kth.id1212.applicationServers.currency.domain;

import javax.persistence.*;

@Entity
@Table(name = "CURRENCY")
public class Currency implements CurrencyDTO {
    @Id
    @Column(name = "ISO_ID")
    private String iso_id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TO_DOLLAR")
    private double to_dollar;

    public Currency( String iso_id, String name, double to_dollar ) {
        this.iso_id = iso_id;
        this.name = name;
        this.to_dollar = to_dollar;
    }

    protected Currency() {}

    @Override
    public String getIsoId() {
        return this.iso_id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getToDollar() {
        return this.to_dollar;
    }

    @Override
    public String toString() {
        return "[iso_id=" + this.iso_id + ", name=" + this.name + ", to_dollar=" + this.to_dollar + "]";
    }
}