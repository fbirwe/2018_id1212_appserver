package se.kth.id1212.applicationServers.currency.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import se.kth.id1212.applicationServers.currency.domain.Conversion;
import se.kth.id1212.applicationServers.currency.domain.ConversionDTO;
import se.kth.id1212.applicationServers.currency.domain.Currency;
import se.kth.id1212.applicationServers.currency.domain.CurrencyDTO;
import se.kth.id1212.applicationServers.currency.repositories.ConversionRepository;
import se.kth.id1212.applicationServers.currency.repositories.CurrencyRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
@Service
public class CurrencyManagement {
    @Autowired
    private CurrencyRepository currenyRepo;

    @Autowired
    private ConversionRepository conversionRepo;

    public CurrencyDTO addCurrency(String iso_id, String name, double to_dollar ) {
        return currenyRepo.save( new Currency( iso_id, name, to_dollar ) );
    }

    private ConversionDTO addConversion(CurrencyDTO from, CurrencyDTO to, double value ) {
        Currency fromEntity = currenyRepo.getOne(from.getIsoId());
        Currency toEntity = currenyRepo.getOne(to.getIsoId());


        return conversionRepo.save( new Conversion( fromEntity, toEntity, value * from.getToDollar() ) );
    }

    public List<ConversionDTO> getLastXConversions( int x ) {
        List<ConversionDTO> conversions = new ArrayList<>();
        int i = 0;

        Iterator<Conversion> iter = conversionRepo.findAll().iterator();

        while(iter.hasNext() && i < x) {
            conversions.add( iter.next() );
        }

        return conversions;
    }

    public List<ConversionDTO> getAllConversions() {
        List<ConversionDTO> conversions = new ArrayList<>();
        
        for( Conversion c : conversionRepo.findAll() ) {
            conversions.add( c );
        }

        return conversions;
    }

    public int getConversionCount() {
        return getAllConversions().size();
    }

    public List<CurrencyDTO> getAllCurrencies() {
        List<CurrencyDTO> currencies = new ArrayList<>();

        for( Currency c : currenyRepo.findAll() ) {
            currencies.add( c );
        }

        return currencies;
    }

    public CurrencyDTO getCurrency( String iso_id ) {
        return currenyRepo.getOne( iso_id );
    }

    public double calculate( CurrencyDTO incomingCurrency, CurrencyDTO outgoingCurrency, double value ) {
        addConversion(incomingCurrency, outgoingCurrency, value);

        double inDollar = value * incomingCurrency.getToDollar();

        return Math.round( inDollar / outgoingCurrency.getToDollar() * 100 ) / 100.0;
    }
}
