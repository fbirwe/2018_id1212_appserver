package se.kth.id1212.applicationServers.currency.presentation;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ConversionForm {

    @NotNull( message = "The value must not be empty")
    @Min( value = 0, message = "The value must be greater than or equal to zero.")
    private Double currency_value;

    @NotNull( message = "The origin currency must not be empty")
    private String from_currency;

    @NotNull( message = "The target currency must not be empty")
    private String to_currency;


    public Double getCurrency_value() {
        return this.currency_value;
    }

    public void setCurrency_value( Double currency_value ) {
        this.currency_value = currency_value;
    }

    public String getFrom_currency() {
        return this.from_currency;
    }

    public void setFrom_currency( String from_currency ) {
        this.from_currency = from_currency;
    }

    public String getTo_currency() {
        return this.to_currency;
    }

    public void setTo_currency( String to_currency ) {
        this.to_currency = to_currency;
    }
}
