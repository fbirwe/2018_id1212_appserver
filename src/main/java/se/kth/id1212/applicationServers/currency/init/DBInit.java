package se.kth.id1212.applicationServers.currency.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import se.kth.id1212.applicationServers.currency.application.CurrencyManagement;
import se.kth.id1212.applicationServers.currency.repositories.CurrencyRepository;

@Component
public class DBInit implements ApplicationListener<ApplicationReadyEvent> {
    /**
     * This event is executed as late as conceivably possible to indicate that
     * the application is ready to service requests.
     */

    @Autowired
    private CurrencyManagement currencyManagement;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        currencyManagement.addCurrency("usd", "US Dollar", 1);
        currencyManagement.addCurrency("eur", "Euro", 1.12928);
        currencyManagement.addCurrency("sek", "swedish krones", 0.109906 );
        currencyManagement.addCurrency("dkk", "danish krones", 0.151261);
    }
}