package se.kth.id1212.applicationServers.currency.presentation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class CurrencyForm {

    @NotNull( message = "The iso must not be empty")
    @Pattern( regexp = "\\w\\w\\w", message = "The iso code must consist of three letters ")
    private String iso_id;

    @NotNull( message = "The name must not be empty")
    private String name;

    @NotNull( message = "The conversion rate must not be empty")
    private Double to_dollar;

    public String getIso_id() {
        return this.iso_id;
    }

    public void setIso_id( String iso_id ) {
        this.iso_id = iso_id;
    }

    public String getName() {
        return this.name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public Double getTo_dollar() {
        return this.to_dollar;
    }

    public void setTo_dollar( Double to_dollar ) {
        this.to_dollar = to_dollar;
    }

}
