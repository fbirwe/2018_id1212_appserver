package se.kth.id1212.applicationServers.currency.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CONVERSIONS")
public class Conversion implements ConversionDTO {
    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "timestamp", updatable = false, nullable = false)
    private long timestamp;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, optional = false)
    private Currency from;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, optional = false)
    private Currency to;

    @Column(name = "value", updatable = false, nullable = false)
    private double value;

    public Conversion( Currency from, Currency to, double value ) {
        this.timestamp = new Date().getTime();
        this.from = from;
        this.to = to;
        this.value = value;
    }

    protected Conversion() {}

    @Override
    public CurrencyDTO getFrom() {
        return from;
    }

    @Override
    public CurrencyDTO getTo() {
        return to;
    }

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "[timestamp=" + this.timestamp + ", from=" + from.getIsoId() + ", to=" + to.getIsoId() + ", value=" + value + "]";
    }


}
